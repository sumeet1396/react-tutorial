import React from 'react';

const Logo = (props) => {
    return (
      <h1>
        <a href="/" title={props.title}>{props.title}</a>
      </h1>
    );
}

export default Logo;
