import React, {Component} from 'react';

/* 
Lifecycle of Components: Each component in React has a lifecycle which you can monitor and manipulate during its three main phases.

The three phases are: 'Mounting', 'Updating', and 'Unmounting'.

1) Mounting: means putting elements into the DOM.
React has four built-in methods that gets called, in this order, when mounting a component:

constructor()
getDerivedStateFromProps()
render()
componentDidMount()
The render() method is required and will always be called, the others are optional and will be called if you define them.

2) Updating: A component is updated whenever there is a change in the component's state or props.

React has five built-in methods that gets called, in this order, when a component is updated:

getDerivedStateFromProps()
shouldComponentUpdate()
render()
getSnapshotBeforeUpdate()
componentDidUpdate()
The render() method is required and will always be called, the others are optional and will be called if you define them.

3) Unmounting: React has only one built-in method that gets called when a component is unmounted:

componentWillUnmount()
*/

class News extends Component {
  constructor(props) {
    super(props);
    this.state = { name: 'lifecycle' };
  }

  /*
    The getDerivedStateFromProps() method is called right before rendering the element(s) in the DOM. This is the natural place to set the state object based on the initial props. It takes state as an argument, and returns an object with changes to the state.
  */
  // static getDerivedStateFromProps(props, state) {
  //   return { name: props.title };
  // }

  /*
    The componentDidMount() method is called after the component is rendered. This is where you run statements that requires that the component is already placed in the DOM. Note: too use this comment the getDerivedStateProps
  */
 
  componentDidMount() {
    setTimeout(() => {
      this.setState({name: "test"})
    }, 5000)
  }


  /*
    In the shouldComponentUpdate() method you can return a Boolean value that specifies whether React should continue with the rendering or not. The default value is true.
  */
  // shouldComponentUpdate() {
  //   return false; // set it true if you wish to update your component
  // }

  changeName = () => {
    this.setState({name: "NewName"});
  }

  /*
    In the getSnapshotBeforeUpdate() method you have access to the props and state before the update, meaning that even after the update, you can check what the values were before the update. If the getSnapshotBeforeUpdate() method is present, you should also include the componentDidUpdate() method, otherwise you will get an error.
  */

  getSnapshotBeforeUpdate(prevProps, prevState) {
    document.getElementById("div1").innerHTML =
    "Before the update, the favorite was " + prevState.name;
  }

  componentDidUpdate() { // The componentDidUpdate method is called after the component is updated in the DOM.
    document.getElementById("div2").innerHTML =
    "The updated favorite is " + this.state.name;
  }

  componentWillUnmount() {
    alert("The component is unmounted.");
  }

  render() {
    return (
      <div>
        <p>Component Life cycle: {this.state.name}</p>
        <button type="button" onClick={this.changeName}>Change color</button>
        <div id="div1"></div>
        <div id="div2"></div>
      </div>
    )
  }
}

export default News;
