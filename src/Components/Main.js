import React from 'react';
import Blog from './Main-blog';
import Gallery from './Main-gallery';
import Clients from './Main-clients';
import News from './News'

const Main = () => {
  return (
    <main>
      <Blog />
      <Gallery />
      <Clients />
      <News title="dummy" />
    </main>
  );
}

export default Main;
