import React, {Component} from 'react';

class Clients extends Component {
  //The constructor() method is called before anything else, when the component is initiated, and it is the natural place to set up the initial state and other initial values.
  constructor() { 
    super();
    this.state = {
      stateName: "Name",
      count: 0,
      max: 100
    }
    this.changeData = this.changeData.bind(this) //whenever we use a method with set state, we need to bind it to the class component

    /* state can only be used when we are using a class component & it is used in a constructor function as a object */
  }

  changeData() {
    this.setState(prevState => { 
      return {
        max: prevState.max - 1
      }
    });

    //prevState is used to identify the previous version of the state before making any changes to it, can use any name instead of prevState
  }

  render() {

    const fireEvent = () => {
      let num = this.state.count;
      num++;
      this.setState({count: num}); //directly modifing the state value

      alert(`${this.state.stateName} ${this.state.count}`);
    }

    return (
      <section className="clients">
        <div className="wrapper">
          <span onMouseOver={() => alert("hovered")}>{this.state.stateName}</span>
          <span>{this.state.max}</span>
          <div>
            <button onClick={fireEvent}>Click Me</button>
            <button onClick={this.changeData}>Change State</button>
          </div>
        </div>
      </section>
    )
  }
}

export default Clients;
