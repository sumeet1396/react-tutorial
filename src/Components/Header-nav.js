import React from 'react';
import '../App.css';

const Nav = () => {

  let nav_list = ['link1','link2','link3','link4','link5'];
  let nav_links = nav_list.map(list => <li key={nav_list.indexOf(list)}><a href="#FIXME">{list}</a></li>)
  return (
    <nav>
      <ul>
         {nav_links}
      </ul>
  </nav>
  );
}

export default Nav;