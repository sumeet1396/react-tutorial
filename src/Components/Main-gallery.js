import React, {Component} from 'react';
import QA from './Main-qa';
import qaData from '../json/QA';

class Gallery extends Component {
  render() {
    let qusetionAnswer = qaData.map((data) => <QA key={data.id} info={data}/>);
    return (
      /* inline styling used as a object string, can only add one property */
      <section className="gallery" style={{backgroundColor: "#888686"}}>
        <div className="wrapper" style={{padding: "100px 0"}}>
          <ul className="question-answer">
            {qusetionAnswer}
          </ul>
        </div>
      </section>
    );
  }
}

export default Gallery;