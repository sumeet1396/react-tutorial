import React from 'react';
import Logo from './Header-logo';
import Nav from './Header-nav';

const Header = () => { //stateless functional component
  return (
    <header>
      <div className="wrapper">
        <Logo title="logo" />
        <Nav />
      </div>
    </header>
  );
}

export default Header;