import React, {Component} from 'react';
import BlogDetails from './Main-blog-details';
import blogPost from '../json/blog-data'

class Blog extends Component {  //statefull class component

  /*
    The render() method is required, and is the method that actually outputs the HTML to the DOM.
  */
  render() {

    const dummyText = `Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum quos a facilis ipsa ad ab culpa corrupti fugiat hic, quis velit eum autem inventore ex, nihil similique enim eius dolorem.`;

    // used an object to store inline css property and value
    const style = {
      padding: "200px 0",
      backgroundColor: "#c1c1c1" // dash '-' is to be replaced with camelCase
    };

    let date = new Date();
    let hrs = date.getHours();
    let heading;

    // conditional text & style changes
    if (hrs < 12) {
      heading = "Morning";
      style.color = "#07456f";
    } else if (hrs >= 12 && hrs <= 17) {
      heading = "Afternoon";
      style.color = "#2e0927";
    } else {
      heading = "Night";
      style.color = "#d90000";
    }

    let multiBlogs = blogPost.map((post) => {
      return (
        <BlogDetails
          key = {post.id.toString()}
          heading = {post.heading}
          para = {post.para}
          imgSrc = {post.imgSrc}
          imgAltText = {post.imgAltText}
        />
      );
    });

    return (
      <section className="blog" style={style}>
        <div className="wrapper">
          <h2>{heading}</h2>
          <ul className="blog-list">
            <BlogDetails
              key = {blogPost[0].id}
              heading = {blogPost[0].heading} //using single value form json data object
              para = {dummyText}
              imgSrc = "/images/test.png"
              imgAltText = "Dexter Image"
            />

            <BlogDetails
              key = "101"
              //conditionl style when a element or data is not present
              para = {dummyText}
              imgSrc = "/images/test.png"
              imgAltText = "Dexter Image 2"
            />

            {/* creating multiple props using json data and map function */}
            {multiBlogs}
          </ul>
        </div>
      </section>
    )
  }
}

export default Blog;
