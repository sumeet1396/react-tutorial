import React, {Component} from 'react';

class BlogDetails extends Component {
  render () {
    return (
      <li key={this.props.info.question.id}>
        <div>
          <h5>Question: {this.props.info.question}</h5>
          <p>Answer: {this.props.info.answer}</p>
          <span>Marks: {this.props.info.marks}</span>
        </div>
      </li>
    )
  }
}

export default BlogDetails;