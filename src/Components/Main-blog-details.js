import React, {Component} from 'react';

class BlogDetails extends Component {
  render () {
    return (
      <li>
        <div className="blog-post">
          <figure>
            <img src={this.props.imgSrc} alt={this.props.imgAltText}/>
          </figure>
          {/* conditional styling if text is not availabe remove the element */}
          <h3 style={{display: this.props.heading ? 'block' : 'none'}}>Title: {this.props.heading}</h3>
          <p>{this.props.para}</p>
        </div>
      </li>
    )
  }
}

export default BlogDetails;